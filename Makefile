# ------------------
# CLOUD-TOOLS-MAKEFILE
# ------------------
#
# MAINTAINER: Bryan Nice


# -------------
# INTERNAL VARIABLES
# -------------

# -------------
# VARIABLES
# -------------

# -------------
# FUNCTIONS
# -------------

# -------------
# TASKS
# -------------
.PHONY: az-cli
az-cli:
	@docker run -it --rm --privileged -v ${HOME}:/root -v /var/run/docker.sock:/var/run/docker.sock entelexeia/cloud-tools:az bash

.PHONY: aws-cli
aws-cli:
	@docker run -it --rm --privileged -v ${HOME}:/root -v /var/run/docker.sock:/var/run/docker.sock entelexeia/cloud-tools:aws bash

.PHONY: gcloud-cli
gcloud-cli:
	@docker run -it --rm --privileged -v ${HOME}:/root -v /var/run/docker.sock:/var/run/docker.sock entelexeia/cloud-tools:gcloud bash