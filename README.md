# Cloud Tools

Repo for images used in the cookbook repo's. These images are designed to run locallying on a dev machine. Not designed for production use.

## Tags
- `base`, `latest` is the base image use by cloud specific image.
- `az` is the azure image used to provision resources.
- `gcloud` is the google cloud image used to provision resources.
- `aws` is the aws cloud image used to provision resources.

The `latest` (or `base`) image has terraform, ansible, kubectl, docker cli, kubeflow cli, and pachyderm cli. The sub-sequent tags have the cloud provider specific cli added to the base.

## Commands

```bash
make az-cli
```

```bash
make aws-cli
```

```bash
make gcloud-cli
```